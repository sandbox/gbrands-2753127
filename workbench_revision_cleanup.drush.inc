<?php

use Drush\Log\LogLevel;

/**
 * Implementation of hook_drush_command().
 */
function workbench_revision_cleanup_drush_command() {
  $items = array();
  $items['workbench-revision-cleanup'] = array(
    'callback' => 'workbench_revision_cleanup_drush_run',
    'description' => 'Drush command to get node count of particular node type.',
    'aliases' => array('wrc'),
    'arguments' => array(
      'types' => 'Comma-separated list of all node types to clean up.',
    ),
    'options' => array(
      'keep' => 'Number of revisions to keep. Will default to 10.',
    ),
    'examples' => array(
      'Clean up all nodes of type page and article' => 'drush wrc page,article',
      'Clean up all nodes of type page, keep 1 revision' => 'drush wrc page,article --keep=1',
    ),
  );
  return $items;
}

/**
 * Run the revision cleanup via Drush.
 */
function workbench_revision_cleanup_drush_run($cs_types = '') {
  $removable = array_keys(node_type_get_names());
  $types = array_intersect(array_filter(explode(',', $cs_types)), $removable);
  if (empty($types)) {
    drush_log(dt('No valid node types specified.'), LogLevel::ERROR);
    return;
  }
  $keep = drush_get_option('keep', 10);
  if ($keep < 1) {
    drush_log(dt('Cannot keep less than 1 revision.'), LogLevel::ERROR);
    return;
  }

  module_load_include('inc', 'workbench_revision_cleanup', 'workbench_revision_cleanup.admin');
  $batch = workbench_revision_cleanup_batch_remove_revisions($types, $keep);
  batch_set($batch);

  $batch =& batch_get();
  $batch['progressive'] = FALSE;

  drush_backend_batch_process();
}
