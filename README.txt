Workbench Revision Cleanup
==========================
The Workbench Revision Cleanup module helps to clean up the revisions for a
site utilizing the Workbench Moderation module. This module helps to ensure
the current drafts, which are newer than the published node, are not removed
during a revision cleanup. All drafts newer than the current, published node
are not considered for revision deletion.


Installation
============

1. Download and install the module as usual.

2. The Workbench Revision Cleanup makes a permission available that allows
   only properly permissioned users to make use of bulk revision cleanup.
   Users with the 'workbench revision cleanup bulk' permission will be able
   to use the tool.

3. Visit "admin/content/workbench-revision-cleanup"

4. Choose your desired content types and settings and run the batch process.

5. Once the batch process has ran, you should specify an ongoing revision
   cleanup per content type via the node type edit form accessible via
   "admin/structure/types".
