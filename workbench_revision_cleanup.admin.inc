<?php
/**
 * @file
 * The batch process that removes the revisions in bulk.
 */

/**
 * Page callback for the workbench revision cleanup batch page.
 */
function workbench_revision_cleanup_remove_revisions_page() {
  $content = array();

  // Grab db size before.
  $size = _workbench_revision_cleanup_db_size();

  // If available, report the current DB size prior to running the batch.
  if ($size) {

    // Set the value so we can calculate savings.
    $before = variable_get('workbench_revision_cleanup_db_before', '');
    $after = variable_get('workbench_revision_cleanup_db_after', '');

    if ($before && $after) {
      $prc = 1 - ($after / $before);
      $percent = number_format($prc, 6);
      $content['info1'] = array(
        '#prefix' => '<p><strong>',
        '#suffix' => '</strong></p>',
        '#markup' => t('The last time this revision process ran. It reduced the DB size by :percent%.',
          array(':percent' => $percent)),
      );
    }

    $db_size = _workbench_revision_cleanup_db_size_info($size);
    $content['info2'] = array(
      '#prefix' => '<p><strong>',
      '#suffix' => '</strong></p>',
      '#markup' => t('The current database size is :size', array(':size' => $db_size['size'] . ' ' . $db_size['type'])),
    );
  }

  $content['form'] = drupal_get_form('workbench_revision_cleanup_remove_revisions_page_form');

  return $content;
}

/**
 * Batch form initiation.
 */
function workbench_revision_cleanup_remove_revisions_page_form($form, &$form_state) {

  $removable = node_type_get_types();

  $options = array();
  foreach ($removable as $remove) {
    $options[$remove->type] = $remove->name;
  }
  asort($options);

  $form['remove_revisions'] = array(
    '#type' => 'select',
    '#title' => t('Select the content types to remove old revisions'),
    '#multiple' => TRUE,
    '#options' => $options,
  );

  $form['keep'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of revisions to keep'),
    '#default_value' => 10,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Begin Removal Process'),
  );

  return $form;
}

/**
 * Submit handler for Batch form initiation.
 */
function workbench_revision_cleanup_remove_revisions_page_form_submit(&$form, &$form_state) {

  $function = 'workbench_revision_cleanup_batch_remove_revisions';

  if (!empty($form_state['values']['remove_revisions'])) {
    // Execute the batch function.
    $batch = $function($form_state['values']['remove_revisions'], $form_state['values']['keep']);
    batch_set($batch);
  }
  else {
    drupal_set_message(t('No content types selected.'));
  }

}

/**
 * Batch function.
 */
function workbench_revision_cleanup_batch_remove_revisions($types, $limit) {
  // Save db size before.
  $size = _workbench_revision_cleanup_db_size();
  if ($size) {
    variable_set('workbench_revision_cleanup_db_before', $size);
  }

  $operations = array();

  // Set up an operations array.
  foreach ($types as $type) {

    $result = db_query('SELECT r.nid, COUNT(r.nid) AS number_of_revisions FROM {node_revision} r LEFT JOIN {node} AS n ON n.nid = r.nid WHERE n.type = :type GROUP BY nid HAVING number_of_revisions > :limit ORDER BY number_of_revisions DESC', array(':type' => $type, ':limit' => $limit));

    foreach ($result as $row) {
      // Each operation is an array consisting of
      // - The function to call.
      // - An array of arguments to that function.
      $operations[] = array(
        'workbench_revision_cleanup_batch_op_remove_revisions',
        array($row->nid, $limit),
      );
    }
  }

  $batch = array(
    'operations' => $operations,
    'finished' => 'workbench_revision_cleanup_batch_remove_revisions_finished',
    'file' => drupal_get_path('module', 'workbench_revision_cleanup') . '/workbench_revision_cleanup.admin.inc',
  );

  return $batch;
}

/**
 * Batch Operation to remove the revisions.
 */
function workbench_revision_cleanup_batch_op_remove_revisions($nid, $limit, &$context) {

  // Use the $context['sandbox'] at your convenience to store the
  // information needed to track progression between successive calls.
  if (empty($context['sandbox'])) {
    $context['sandbox'] = array();
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = $nid;
    $context['sandbox']['current_revision'] = 0;

    // Load the current live revision id if applicable
    // We do not want to remove any revisions that are newer than
    // the current version.
    if ($current_vid = db_query('SELECT vid FROM {workbench_moderation_node_history} WHERE nid = :nid AND published = :published', array(':nid' => $nid, ':published' => 1))->fetchField()) {
      $context['sandbox']['current_vid'] = $current_vid;
      $num_revisions = db_query('SELECT COUNT(*) FROM {node_revision} n WHERE n.nid = :nid AND n.vid < :vid', array(':nid' => $nid, ':vid' => $current_vid))->fetchField();
    }
    else {
      $num_revisions = db_query('SELECT COUNT(*) FROM {node_revision} n WHERE n.nid = :nid', array(':nid' => $nid))->fetchField();
    }
    $context['sandbox']['max'] = $num_revisions - $limit;
  }

  $process_each_round = 10;

  // Retrieve the next group of nids.
  $query = db_select('node_revision', 'n');
  $query->fields('n', array('nid', 'vid', 'title'))
    ->orderBy('n.vid', 'ASC');
  $query->condition('n.nid', $nid, '=');

  // Make sure the new, unpublished drafts are not destroyed.
  if (!empty($context['sandbox']['current_vid'])) {
    $query->condition('n.vid', $context['sandbox']['current_vid'], '<');
  }

  $query->where('n.vid > :vid', array(':vid' => $context['sandbox']['current_revision']));

  $result = $query->range(0, $process_each_round)->execute();

  $context['sandbox']['results'][] = $result->rowCount();

  foreach ($result as $row) {

    // Only remove it if more than 10.
    if ($context['sandbox']['progress'] >= $context['sandbox']['max']) {
      break;
    }
    try {
      node_revision_delete($row->vid);
    }
    catch (Exception $e) {
      $msg = $e->getMessage();
    }

    // Store some results for post-processing in the 'finished' callback.
    // The contents of 'results' will be available as $results in the
    // 'finished' function (in this example, batch_example_finished()).
    $context['results'][0] = $row->nid . ' : ' . check_plain($row->title);

    // Update our progress information, if all old revisions were deleted.
    $context['sandbox']['progress']++;
    $context['sandbox']['current_node'] = $row->nid;
    $context['sandbox']['current_revision'] = $row->vid;

    $context['message'] = t('@current of @max (@percent%): @title', array(
      '@current' => $context['sandbox']['progress'],
      '@max' => $context['sandbox']['max'],
      '@percent' => number_format((($context['sandbox']['progress'] / $context['sandbox']['max']) * 100), 2),
      '@title' => $row->title . ((!empty($msg) ? ' ' . $msg : '')),
    ));
  }

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = ($context['sandbox']['progress'] >= $context['sandbox']['max']);
  }

}

/**
 * Batch finished callback.
 */
function workbench_revision_cleanup_batch_remove_revisions_finished($success, $results, $operations) {
  if ($success) {
    // Here we could do something meaningful with the results.
    // We just display the number of nodes we processed...
    drupal_set_message(t('The final result was "%final"', array('%final' => end($results))));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    drupal_set_message(t('An error occurred while processing the removal of revisions.'), 'error');
  }

  // Grab db size after.
  $size = _workbench_revision_cleanup_db_size();

  if ($size) {
    // Set the value so we can calculate savings.
    variable_set('workbench_revision_cleanup_db_after', $size);
  }
}

/**
 * Helper to retrieve database size.
 *
 * @return int
 *   Size of DB in bytes
 */
function _workbench_revision_cleanup_db_size() {

  // Database size = table size + index size.
  $rows = db_query("SHOW TABLE STATUS");
  $db_size = 0;
  foreach ($rows as $row) {
    $db_size += $row->Data_length + $row->Index_length;
  }

  return $db_size;
}

/**
 * Helper to convert DB size to a more friendly output.
 */
function _workbench_revision_cleanup_db_size_info($db_size) {

  $bytes = array('KB', 'KB', 'MB', 'GB', 'TB');

  if ($db_size < 1024) {
    $db_size = 1;
  }

  for ($i = 0; $db_size > 1024; $i++) {
    $db_size /= 1024;
  }

  $db_size_info['size'] = number_format($db_size, 4);
  $db_size_info['type'] = $bytes[$i];

  return $db_size_info;
}
